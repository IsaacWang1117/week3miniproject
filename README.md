# Week 3 Mini-Project: S3 Bucket Creation with AWS CDK and CodeWhisperer

This project demonstrates the creation of an AWS S3 bucket using the AWS Cloud Development Kit (CDK) with the assistance of AWS CodeWhisperer for generating the CDK code. 

## Project Setup

Create a new directory for my project and initializing a CDK application

```bash
mkdir hello-cdk
cd hello-cdk
```

Initialize the app by using the cdk init command and specify the desired template ("app") and programming language

```bash
cdk init app --language typescript
```

### Implementing S3 Bucket

Navigate to **lib/hello-cdk-stack.ts** to modify the stack for S3 bucket creation. Utilize AWS CodeWhisperer for generating CDK code snippets for advanced features like versioning and encryption.

Deploy the CDK stack to your AWS account

```bash
cdk synth
cdk deploy
```

## Use of CodeWhisperer

### Question I asked

"How to add versioning and encryption to the S3 bucket I created?"

### Versioning

CodeWhisperer suggested a line of code intended to enable versioning. However, upon integration, i encountered a 'not exist' error indicating that the suggested code snippet might not have been compatible with my current setup.

I decided to adhere to the original setup in tutorial. 

### Encryption

The encryption suggestion from CodeWhisperer was directly applicable and enhanced our bucket's security effectively. This success story underscores the utility of CodeWhisperer in facilitating secure and efficient cloud infrastructure setups.

### Key Takeaways

My interactions with AWS CodeWhisperer yielded mixed but insightful outcomes. It demonstrated the potential of AI-assisted coding in cloud development, while also highlighting the importance of contextual adaptation of AI-generated suggestions.

### Screenshot of AWS S3 bucket using the AWS CDK with the assistance of AWS CodeWhisperer

![event JSON](./S3_bucket_scrrenshot_for_week3.png)